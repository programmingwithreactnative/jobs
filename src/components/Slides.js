import React, {Component} from 'react';
import {View, Text, ScrollView, Dimensions} from 'react-native';
import {Button} from 'react-native-elements';

const SCREEN_WIDTH = Dimensions.get('window').width;
class Slides extends Component{
    /** 
     * if we bind the onComplete method with paranthesis like so: this.props.onComplete()
     * this method will be firead as soon as the button is rendered. which is not what we want.
    */
    renderLastSlide(index){
        if(index === this.props.data.length - 1){
            return(
                <Button 
                title="Get Started" 
                raised
                buttonStyle={styles.buttonStyle}
                onPress={this.props.onComplete}
                />
            );
        }
    }
    renderSlides(){
        /** 
         * below naming of "slide" inside the map function is arbitrary.
         * text is unique and does not change between render passes, so we can use if for key
         * in our list. since we are building a list we must provide a key, remember.
        */
        return this.props.data.map((slide, index)=>{
            return(            
                <View key={slide.text} style={[styles.slideStyle,{backgroundColor:slide.color}]}>
                    <Text style={styles.textStyle}>
                        {slide.text}
                    </Text>
                    {this.renderLastSlide(index)}
                </View>
            );
        });
    }
    render(){
        return(
            /** 
             * in JSX it is enough to just define the property. it will be interpretted as true.
             * below will be interpretted as horizontal=true
             * flex:1 is used to make the scroll view fill up the screen
             * to get our welcome screen, here in this scroll view the critical bits are :
             *  1- horizontal
             *  2- width: SCREEN_WIDTH
             *  3- pagingEnabled
             *  4- flex:1
            */
            <ScrollView
            horizontal
            pagingEnabled
            style={{flex:1}}
            >
                {this.renderSlides()}
            </ScrollView>
        );
    }
}
const styles = {
    textStyle:{
        fontSize:30,
        textAlign:'center',
        color:'white',
        paddingBottom: 50
    },
    slideStyle:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        width: SCREEN_WIDTH
    },
    buttonStyle:{
        
    }
};
export default Slides;