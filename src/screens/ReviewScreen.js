import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {Button} from 'react-native-elements';

class ReviewScreen extends Component{
    /** 
     * whenever a navigator (stack or tab navigator) is about to show a component, its gonna look at that component's
     * "navigationOptions" property. we define this property as a class-level (static) property. (if it was not static, it
     * would be an instance property.)
     * 
     * note: we can import Platform from react-native and run platform specific code
     * example:
     * style:{
     *  marginTop: Platform.OS === 'android' ? 30 : 0
     * }
    */

    static navigationOptions = ({ navigation }) => {
        return {
            headerTitle: 'Review Jobs',
            headerRight: (
                <Button 
                title='Settings' 
                onPress={() => navigation.navigate('settings')} 
                />
            ),
            headerStyle:{
                backgroundColor:'red'
            }
        };
    }

    render(){
        return(
            <View>
                <Text>
                    Review Screen
                </Text>
            </View>
        );
    }
}

export default ReviewScreen;