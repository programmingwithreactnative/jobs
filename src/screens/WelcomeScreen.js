import React, {Component} from 'react';
import {View, Text} from 'react-native';
import Slides from '../components/Slides';

const SLIDE_DATA =[
    {text:'Welcome to this app.', color:'#03a9f4'},
    {text:'Use this app to have fun.', color:'#009688'},
    {text:'set your location, start your journey.', color:'#03a9f4'},
];

class WelcomeScreen extends Component{
    /** 
     * since this is a callback we will bind the context as "this". if we define a fat arrow function
     * we don't need ".bind(this)" part. arrow function like so:
     * 
     * onSlidesComplete = () => {
     * 
     * }
    */
    onSlidesComplete(){
        /** 
         * whenever we render a compoenent with react navigation library, react navigation will 
         * automatically pass down a set of props called "navigation" to the component that it renders.
         * inside this navigation object we have "navigate" method that we can use to programmatically
         * navigate between screens.
         * see that inside App.js WelcomeScreen is being rendered by react navigation through 
         * the tab navigator.
        */
        this.props.navigation.navigate('auth');
    }
    render(){
        return(
            <Slides data={SLIDE_DATA} onComplete={this.onSlidesComplete.bind(this)}/>
        );
    }
}

export default WelcomeScreen;