import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {
    createBottomTabNavigator,
    createAppContainer,
    createStackNavigator
} from 'react-navigation';
import AuthScreen from './screens/AuthScreen';
import WelcomeScreen from './screens/WelcomeScreen';
import MapScreen from './screens/MapScreen';
import DeckScreen from './screens/DeckScreen';
import ReviewScreen from './screens/ReviewScreen';
import SettingsScreen from './screens/SettingsScreen';

// class App extends Component{
//     render(){
//         return(
//             <View>
//                 <MainNavigator />
//             </View>
//         );
//     }
// }

/** 
 * in the navigator we define keys (welcome, auth..) and which screens should be displayed for each key.
 * we can also use keys to programmatically navigate between the screens.
 * Stack navigation wont work if you dont do this:
 *  1- npm install react-native-gesture-handler
 *  2- react-native link react-native-gesture-handler
 *  3- react-native run-android
 *  4- npm start
*/
const ReviewStack = createStackNavigator({
    review:{screen:ReviewScreen},
    settings:{screen:SettingsScreen},
},
{
    initialRouteName:'review'
});
const MainNavigator = createBottomTabNavigator({
    map:{screen:MapScreen},
    deck:{screen:DeckScreen},
    review:ReviewStack
});
const PrimaryNavigator = createBottomTabNavigator({
    welcome: {screen: WelcomeScreen},
    auth: {screen:AuthScreen},
    main: MainNavigator

});
export default createAppContainer(PrimaryNavigator);